# Python Package

Sometimes it is instructive to write a python tool which can be used anywhere.
For example we may have the following setup
```
base
|- package
   |- __init__.py
   |- lib1.py
   |- lib2.py
|- scripts
   |- compile.sh
util.py
```

Suppose we want the following to happen:
1. `lib1.py` can import `lib2.py` and call its functions.
2. `util.py` can import `lib1.py` and call its functions.
3. `lib1.py`'s main function can use `lib2.py` (Usually these contain unit
		tests)

This is a very involved Python import problem and the best solution is to
execute the module with only `-m` calls. An example is given in `test.sh`.
i.e.
```bash
# PWD must be set to package/..
python3 -m package.lib1
python3 -m package.lib2
```


Credit:
1. [Relative imports in Python 3](https://stackoverflow.com/questions/16981921/relative-imports-in-python-3)
